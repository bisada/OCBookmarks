# [v1.24](https://gitlab.com/bisada/OCBookmarks/-/tags/v1.24)
Entering a bookmark in dark theme is almost impossible.
- [dark theme Issues](https://gitlab.com/bisada/OCBookmarks/-/issues/99)

# [v1.23](https://gitlab.com/bisada/OCBookmarks/-/tags/v1.23)

Since the nextcloud app has minSDK=23, we dont need to support it any longer.
- [Drop support to 21 :](https://gitlab.com/bisada/OCBookmarks/-/issues/95)
- minor changes.

# [v1.22](https://gitlab.com/bisada/OCBookmarks/-/tags/v1.22)

Release v1.22 New UI fix

**Clean UI and Fast lane updates** 

<img src="/uploads/d8f90154adfced218dd50fc2ff198b0e/3.png"  width="550" height="950">
<img src="/uploads/3125e8d3f9cf252e68cedb3f051c4b2a/4.png"  width="550" height="950">
<img src="/uploads/8123403aa150d1aa8de055404bc13e74/4.png"  width="550" height="950">


# [v1.21](https://gitlab.com/bisada/OCBookmarks/-/tags/v1.21)

Release v1.21 is to update the f-droid.
- fdroid fix.
- translation fix.

# [v1.20](https://gitlab.com/bisada/OCBookmarks/-/tags/v1.20)

Release: V1.20 Minor fix on quick add and reauthenticate fix

- [API fix: now user does need to reauthenticate the app every time opes the app.](https://gitlab.com/bisada/OCBookmarks/-/issues/68)
- [Quickadd Fix: Check for existing bookmark before adding if exists.](https://gitlab.com/bisada/OCBookmarks/-/merge_requests/69)

# [v1.19](https://gitlab.com/bisada/OCBookmarks/tags/v1.18)

This release is fix for SSO.
- [SSO issue fixed](https://gitlab.com/bisada/OCBookmarks/-/issues/83)

# [v1.18](https://gitlab.com/bisada/OCBookmarks/tags/v1.18)
This release is major release. Thanks to [newhinton](https://gitlab.com/newhinton). The following feature are released:
- One click Quick Sharing.
- Folder structure.
- CI FIX.
- Multiple functionality improvements and remove unnecessary methods.
- Switching between multiple accounts.
- The following Issues has been addressed in the following release:
  - [Save in folders](https://gitlab.com/bisada/OCBookmarks/-/issues/25) 
  - [Switching between multiple SSO accounts](https://gitlab.com/bisada/OCBookmarks/-/issues/79)
  - [widget and tags improvement](https://gitlab.com/bisada/OCBookmarks/-/issues/7)
  - [[feature request] save in folders](https://gitlab.com/bisada/OCBookmarks/-/issues/25)
  - [return](https://gitlab.com/bisada/OCBookmarks/-/issues/71)
  - [Configurable default view](https://gitlab.com/bisada/OCBookmarks/-/issues/1)
  - [Folder support?](https://gitlab.com/bisada/OCBookmarks/-/issues/61)
  - [Where are the bookmarks folders?](https://gitlab.com/bisada/OCBookmarks/-/issues/68)
  - [CI pipeline is broken](https://gitlab.com/bisada/OCBookmarks/-/issues/78)
  - [Drawer home icon missing](https://gitlab.com/bisada/OCBookmarks/-/issues/52)

# [v1.17](https://gitlab.com/bisada/OCBookmarks/tags/v1.17)
- Introducing SSO again.
- minor improvements in the app.
- The following issues are closed:
  - [[Feature request] Single sign-on](https://gitlab.com/bisada/OCBookmarks/-/issues/27)
  - [unable to login](https://gitlab.com/bisada/OCBookmarks/-/issues/60)
  - [Nextcloud Bookmarks issue with TLSv1.3](https://gitlab.com/bisada/OCBookmarks/-/issues/42)
  - [support http and https connection](https://gitlab.com/bisada/OCBookmarks/-/issues/30)

# [v1.16](https://gitlab.com/bisada/OCBookmarks/tags/v1.16)

- SSO Removed as requested #67

# [v1.15](https://gitlab.com/bisada/OCBookmarks/tags/v1.15)

- Bookmark description &  Title depend on API.
- Bookmarks can now exported and saved to phone local.

# [v1.14](https://gitlab.com/bisada/OCBookmarks/tags/v1.14)

- Refactor of README.md

# [v1.13](https://gitlab.com/bisada/OCBookmarks/tags/v1.13)

- SSO initial release. 

# [v1.12](https://gitlab.com/bisada/OCBookmarks/tags/v1.12)

- New Login screen design. Show/Hide password in login screen. 

# [v1.11](https://gitlab.com/bisada/OCBookmarks/tags/v1.11)

- Added initial Drawer

# [v1.10](https://gitlab.com/bisada/OCBookmarks/tags/v1.10)

- Much waited V3 Bookmark API version fixed.

# [v1.9](https://gitlab.com/bisada/OCBookmarks/tags/v1.9)

- Can not Clear description fix!

# [v1.8](https://gitlab.com/bisada/OCBookmarks/tags/v1.8)

- V3 API Fix!
- This is an intermediate release!!
- Known issues are documented here : <https://github.com/nextcloud/bookmarks/issues/1012> 

# [v1.6](https://gitlab.com/bisada/OCBookmarks/tags/v1.6)

- Latest target version update
- Fix icons
- Gradle update.

# [v1.5](https://gitlab.com/bisada/OCBookmarks/tags/v1.5)

- Fix several translation
- Fix icons


# [v1.2](https://gitlab.com/bisada/OCBookmarks/tags/v1.2)

### Fix
- Fixed icons in menu
- fixed following / problem in log in


# [v1.1](https://gitlab.com/bisada/OCBookmarks/tags/v1.1)

### New
- Add support for owncloud (just the cooperate identity).
- add proguard (minify)
- Russian translation

### fixes
- spelling fixes
- allow to update tags


# [v1.0](https://gitlab.com/bisada/OCBookmarks/tags/v1.0)
Initial release providing general functionality.